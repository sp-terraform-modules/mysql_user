resource "random_password" "db-password" {
  length           = 16
  special          = false
}

resource "mysql_user" "db-user" {
  user               = var.username
  host               = "%"
  plaintext_password = random_password.db-password.result
}

resource "mysql_grant" "db-user" {
  for_each = toset(var.db_name)
  user       = var.username
  host       = "%"
  database   = each.value
  privileges = ["ALL PRIVILEGES"]
  depends_on = [
    mysql_user.db-user
]
}